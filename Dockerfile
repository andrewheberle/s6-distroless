ARG VARIANT=base
ARG BASE=11

FROM debian:$BASE AS s6

ARG S6=v2.2.0.3
ARG ARCH=amd64

RUN apt-get -qy update && \
    apt-get -qy install curl && \
    curl -L -o /tmp/s6-overlay-installer "https://github.com/just-containers/s6-overlay/releases/download/${S6}/s6-overlay-${ARCH}-installer" && \
    chmod +x /tmp/s6-overlay-installer && \
    mkdir /s6 && \
    /tmp/s6-overlay-installer /s6

FROM gcr.io/distroless/$VARIANT-debian$BASE

COPY --from=s6 /s6 /

ENTRYPOINT [ "/init" ]