# s6-distroless

This is a container image based on the "Distroless" images here:

[https://github.com/GoogleContainerTools/distroless](https://github.com/GoogleContainerTools/distroless)

The only additional package installed is "S6 Overlay" from here:

[https://github.com/just-containers/s6-overlay](https://github.com/just-containers/s6-overlay)

## Usage

This image is intended to be used as a base image for your own containers where
you require the minimal attack surface of a "Distroless" container but also
need the initialisation and service management of "S6 Overlay"

## Examples

An example use of this image is in the [Example](example) directory.