# Example

This is an example Golang "Hello World!" web server that runs in a container based on
either the "base" (by default) or "static" variants of the "s6-distroless" image.
